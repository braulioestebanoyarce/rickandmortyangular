import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ICharacters } from 'src/app/interfaces/ICharacter';
import { tap } from 'rxjs';


@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {
  characters: ICharacters = {
    info: {
      count: 0,
      pages: 0,
      next: '',
      prev: null
    },
    results: []
  };
  page!: number;
  pageNum = 1;
  showButtom = false;
  name!: string;

  constructor(private dataService: DataService) {

  }


  ngOnInit(): void {
    this.dataService.getCharacters().subscribe(characters => this.characters = characters);
    this.getCharacter();

  }

  getCharacter(): void {
    if (this.name != " " && this.name != "") {
      this.dataService.getCharacter(this.name).pipe(
        tap((chars1) => {
          //console.log(chars1);
          this.characters.results = chars1.results
        })).subscribe();
    }
  }



  moreCharacters(): void {
    if (this.pageNum < 42) {
      this.pageNum++;
      this.dataService.getMoreCharacters(this.pageNum)
        .pipe(
          tap((chars1) => {
            this.characters.results = chars1.results;
          })
        ).subscribe();
    }
  }

  backCharacters(): void {
    if (this.pageNum > 1) {
      this.pageNum--;
      this.dataService.getMoreCharacters(this.pageNum)
        .pipe(
          tap((chars1) => {
            this.characters.results = chars1.results;
          })
        ).subscribe();
    }
  }


}
