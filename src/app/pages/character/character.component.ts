import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ActivatedRoute } from '@angular/router';
import { ICharacters } from 'src/app/interfaces/ICharacter';
import { Result } from '../../interfaces/ICharacter';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {
  character!: Result;


  constructor(private dataService: DataService,
    private activatesRouter: ActivatedRoute){  }

  ngOnInit(): void {
    this.getCharacterById();
  }

  getCharacterById() {
    this.activatesRouter.paramMap.subscribe(param => {
      let id = Number(param.get('id'));

      this.dataService.getCharacterById(id).subscribe(character => this.character = character)
    });
  }

}
