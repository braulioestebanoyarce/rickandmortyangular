import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs';
import { ICharacters } from 'src/app/interfaces/ICharacter';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  character: ICharacters = {
      info: {
        count: 0,
        pages: 0,
        next: '',
        prev: null
      },
      results: []
    };
  search!: string ;

  constructor(private dataService: DataService,private activatesRouter: ActivatedRoute) {}
  
  ngOnInit() : void{
    
  }

  getCharacters() {
    if (this.search != " "  ) {
      this.dataService.getCharacter(this.search).pipe(
        tap((chars1) => {
          console.log(chars1);
          this.character.results = chars1.results
        })).subscribe();
    }
  }

}
