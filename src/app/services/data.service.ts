import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import {  ICharacters } from 'src/app/interfaces/ICharacter';
import { Result } from '../interfaces/ICharacter';

@Injectable({
  providedIn: 'root'
})
export class DataService {
 

  constructor(private http: HttpClient) { }

  getCharacters(): Observable<ICharacters>{
    return this.http.get<ICharacters>('https://rickandmortyapi.com/api/character');
  }

  getCharacter(name: string): Observable<ICharacters>{
   
    return this.http.get<ICharacters>(`https://rickandmortyapi.com/api/character?name=${name}`);

  }

  getCharacterById(id: number): Observable<Result>{
    return this.http.get<Result>(`https://rickandmortyapi.com/api/character/${id}`);
  }

  getMoreCharacters(pageNumber: number): Observable<ICharacters>{
    return this.http.get<ICharacters>(`https://rickandmortyapi.com/api/character?page=${pageNumber}`);
  }
}
